# SPDX-FileCopyrightText: 2021 ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## ERIS stores backed by Tkrzw databases.

import std/asyncfutures
import tkrzw
import eris

export tkrzw.RW, tkrzw.OpenOption

type DbmStore* = ref object of ErisStoreObj
  dbm*: HashDBM
  ops: Operations

method put(s: DbmStore; r: Reference; f: PutFuture) =
  if Put notin s.ops: raise newException(IOError, "put denied")
  s.dbm.set(r.bytes, f.mget, false)
  complete f

method get(s: DbmStore; r: Reference): Future[seq[byte]] =
  if Get notin s.ops: raise newException(IOError, "get denied")
  result = newFuture[seq[byte]]("dbmGet")
  try:
    var blk = s.dbm[r.bytes]
    assert(blk.len > 0)
    result.complete(blk)
  except:
    result.fail(newException(KeyError, "reference not in store"))

method close(ds: DbmStore) =  close(ds.dbm)

proc newDbmStore*(dbm: HashDBM; ops = {Get,Put}): DbmStore =
  ## Open a store using a hash hatabase backed by file.
  DbmStore(dbm: dbm, ops: ops)

proc newDbmStore*(dbFilePath: string; ops = {Get,Put}; opts: set[OpenOption] = {}): DbmStore =
  ## Open a store using a hash hatabase backed by file.
  var
    rw = if Put in ops: writeable else: readonly
    dbm = newDBM[HashDBM](dbFilePath, rw, opts)
  newDbmStore(dbm, ops)
