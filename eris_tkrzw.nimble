# Package

version       = "0.4.1"
author        = "Emery Hemingway"
description   = "Tkrzw database backend for ERIS"
license       = "Unlicense"
srcDir        = "src"
backend       = "cpp"


# Dependencies

requires "nim >= 1.4.0", "eris >= 0.4.0", "tkrzw >= 0.1.2"

import distros
if detectOs(NixOS):
  foreignDep "tkrzw"
